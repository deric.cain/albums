import React from 'react';
import { Text, View, Image, Linking } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

const AlbumDetail = ({ album }) => {

  const {
    title,
    artist,
    thumbnail_image,
    image,
    url
  } = album;

  const {
    headerStyle,
    imageStyle,
    imageContainer,
    titleText,
    albumImage
  } = styles;

  return (
      <Card>

        <CardSection>
          <View style={imageContainer}>
            <Image
                style={imageStyle}
                source={{ uri: thumbnail_image }}
            />
          </View>
          <View style={headerStyle}>
            <Text style={titleText}>{title}</Text>
            <Text>{artist}</Text>
          </View>
        </CardSection>

        <CardSection>
          <Image
              style={albumImage}
              source={{ uri: image }}
          />
        </CardSection>

        <CardSection>
          <Button onPress={() => Linking.openURL(url)}>
            Buy me!
          </Button>
        </CardSection>
      </Card>
  )
};

const styles = {
  headerStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  imageStyle: {
    height: 50,
    width: 50,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  albumImage: {
    height: 300,
    width: null,
    flex: 1,
  },
  titleText: {
    fontSize: 18,
  }
};

export default AlbumDetail;
