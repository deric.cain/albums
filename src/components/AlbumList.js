import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import AlbumDetail from './AlbumDetail';

export default class AlbumList extends Component {

  state = {
    albums: []
  };

  componentWillMount() {
    fetch('https://rallycoding.herokuapp.com/api/music_albums')
        .then((response) => {
          response.json().then(albums => this.setState({albums: albums}));
        });
  }

  renderAlbums() {
    return this.state.albums.map(album =>
        <AlbumDetail album={album} key={album.title} />
    )
  }
  
  render() {
    return (
      <ScrollView>
        {this.renderAlbums()}
      </ScrollView>
    );
  }
}